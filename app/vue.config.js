const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  // @ts-ignore
  outputDir: (! process.env.VUE_APP_OUTPUT_DIR) ? 'dist/html' : process.env.VUE_APP_OUTPUT_DIR,
  devServer: {
    watchOptions: {
      ignored: "/node_modules/",
      host: "0.0.0.0",
      poll: 500
    },
  },
  configureWebpack: config => ({
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: 'sitemap.xml',
            to: '',
            flatten: true,
            transform(content) {
              const BASE_URL_REGEX = /(\$BASE_URL)/g;
              const LASTMOD_REGEX = /(\$LASTMOD)/g;
              const moment = require('moment');

              return content
                .toString()
                .replace(BASE_URL_REGEX, process.env.VUE_APP_BASE_URL)
                .replace(LASTMOD_REGEX, moment().format());
            },
          }
        ],
      })
    ],
    optimization: {
      usedExports: true,
      splitChunks: {
        chunks: 'async',
        minSize: 30000,
        maxSize: 0,
        minChunks: 1,
        maxAsyncRequests: 6,
        maxInitialRequests: 4,
        automaticNameDelimiter: '~',
        cacheGroups: {
          defaultVendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    },
    resolve: {
      alias: {
        '@core': path.resolve(__dirname, 'node_modules/lenderkit-webapp/src'),
        "@autoinvestment": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-autoinvestment'),
        "@roundup-autoinvestment": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-roundup-autoinvestment'),
        "@comments": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-comments'),
        "@plaid": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-plaid'),
        "@siterequest": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-siterequest'),
        "@donation": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-donation'),
        "@secondarymarket": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-secondarymarket'),
        "@debt": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-debt'),
        "@equity": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-equity'),
        "@indicatedinterests": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-indicatedinterests'),
        "@reward": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-reward'),
        "@socials-auth": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-socials-auth'),
        "@investor-categorization": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-investor-categorization'),
        "@regulations-usa": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-regulations-usa'),
        "@mangopay": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-mangopay'),
        "@gdpr": path.resolve(__dirname, 'node_modules/lenderkit-webapp-module-gdpr')
      },
    },
    module: {
      rules: [
        {
          test: [/\.ts$/, /\.vue$/, /\.js$/],
          use: [
            {
              loader: path.resolve(__dirname, 'node_modules/lenderkit-webapp/importsLoader.js'),
              options: {
                replace: {
                  '@core/config': '@/config',
                  '@core/config/modules.config': '@/config/modules.config',
                }
              },
            },
          ],
        },
      ],
    },
  }),
  runtimeCompiler: true,
  productionSourceMap: false
};
