/* NOTE! This file contains special comments fot autosetup.
  If you add any optional module to webapp application, please surround all the module lines with the next comments
  //module-{same-as-appropriate-backend-module-name}-lines-begin fot lines start
  //module-{same-as-appropriate-backend-module-name}-lines-end fot lines finish
  Note, that module-module comments has NO space after //
*/

//module-autoinvestment-lines-begin
import AutoInvestmentModule from '@autoinvestment/Installer';
//module-autoinvestment-lines-end


//module-comments-lines-begin
import CommentsModule from '@comments/Installer';
//module-comments-lines-end



//module-debt-lines-begin
import Debt from '@debt/Installer';
//module-debt-lines-end



//module-socials-auth-lines-begin
import SocialsAuth from '@socials-auth/Installer';
//module-socials-auth-lines-end

//module-regulations-usa-lines-begin
import RegulationsUSA from '@regulations-usa/Installer';
//module-regulations-usa-lines-end

//module-gdpr-lines-begin
import GDPR from '@gdpr/Installer';
//module-gdpr-lines-end

export const enabledModules = [
//module-autoinvestment-lines-begin
  'autoinvestment',
//module-autoinvestment-lines-end


//module-comments-lines-begin
  'comments',
//module-comments-lines-end





//module-debt-lines-begin
  'debt',
//module-debt-lines-end



//module-socials-auth-lines-begin
  'socials-auth',
//module-socials-auth-lines-end

//module-gtm-lines-begin
  'gtm',
//module-gtm-lines-end
//module-sentry-lines-begin
  'sentry',
//module-sentry-lines-end

];

export default [
//module-autoinvestment-lines-begin
  AutoInvestmentModule,
//module-autoinvestment-lines-end


//module-comments-lines-begin
  CommentsModule,
//module-comments-lines-end



//module-debt-lines-begin
  Debt,
//module-debt-lines-end



//module-socials-auth-lines-begin
  SocialsAuth,
//module-socials-auth-lines-end

//module-regulations-usa-lines-begin
  RegulationsUSA,
//module-regulations-usa-lines-end

//module-gdpr-lines-begin
  GDPR,
//module-gdpr-lines-end
];
