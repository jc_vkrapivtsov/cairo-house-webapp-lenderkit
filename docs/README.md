# LenderKit-based Project

[LenderKit](https://lenderkit.com/) is an enterprise-level white-label crowdfunding platform developed by [JustCoded](https://justcoded.com/).

LenderKit WebApp Starter is a skeleton crowdfunding application based on 
  [LenderKit Platform](https://github.com/lenderkit-dev/webapp)
  and [Vue.js](https://vuejs.org/).

## Getting Started

* [Before You start](before-you-start.md). Get basic knowledge of technologies being used.
* [Requirements](requirements.md). Know how to meet server requirements.
* [Installation](installation.md) +-
* [Docker Containers](docker-containers.md)
* [Makefiles Reference](makefile-reference.md)
* [Starting New Project](start-new-project.md)
* [Development process](development-process.md)

## Digging Deeper

* [Docker Containers Details](docker-containers-details.md) -
* [Troubleshooting](troubleshooting.md) -
