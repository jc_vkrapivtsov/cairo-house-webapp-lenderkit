# LenderKit WebApp Starter

LenderKit WebApp Starter is a skeleton crowdfunding application based on 
  [LenderKit Platform](https://github.com/lenderkit-dev/webapp)
  and [Vue.js](https://vuejs.org/).

Full documentation is at [docs/README.md](docs/README.md)

## Requirements
 - Node.js 10 (NOT 12!!)
 - Nginx Latest
 - _for quick start we recommend to use Docker_
    * Docker CE 19+
    * Docker Compose 1.23.2+

If you are working on MacOS, then you need the additional software:
 - Vagrant
 - Virtual Box
 
Read more detailed information about the requirements [here](requirements.md).

## Code structure

In this document we will use 2 terms: **root level** and **application level**. 
Please take your attention on the scheme below:

```
/                   # ROOT LEVEL
 |- /api                  # WebApp API Server
 |- /app            # APPLICATION LEVEL
   |- /public             # contains static resources
   |- /src                # the root folder of the source files
     |- /config           # contains application and theme config files
     |- /views            # the root folder for the application views
 |- /build                # dev Docker and CI configuration  
 |- /configs              # nginx and SSL configs
 |- /docs                 # detailed documentation
 |- /runtime              # temporary files and docker containers storage volumes
 |- .env                  # root level env configuration
 |- docker-compose*.yml   # docker compose containers config files
 |- Makefile              # root level Make helper
```

## Installation

### Installation helpers

To simplify docker operations, installation/updating process the project use [GNU Make](https://en.wikipedia.org/wiki/Make_(software)) util, configured inside Makefile's.
Makefile contains "targets" - a predefined set or "rules" (commands) to be executed.

By default `make` util executes `info` target, which list all available operations:

```
make
```

There are **3 Makefile's** in the project:

* `/Makefile` or **root level Makefile**.  
This file contains targets to configure your server environment and operate with Docker containers. 
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant CLI).
* `/app/Makefile` or **application level Makefile**.  
This file contains targets to operate with your JavaScript application. It should be called **inside Node containers** (app). 
`/app` folder is mounted as a docroot `/var/www/html` to docker node.js containers, so it's the only available Makefile inside the containers. 
In general this Makefile contains application install/update scripts calls.
* `/api/Makefile` or **WebApp API level Makefile**.  
This file contains targets to operate with WebApp API Server.

### Quick install

To init docker containers and run LenderKit project you can use root level Makefile to do everything for you.

#### 1. Init your environment

After download you need to init your project with your own docker-configuration `/.env` file, docker-compose files and nginx server config.

To copy all necessary files and get information where you can find your local configs run:  
_* We recommend to run this command in your original OS, and NOT inside Vagrant VM if you have Mac OS._ 

`make init-dev`

By default, **you have the working configuration files** and you can leave everything as is.

**Environment variables**

Inside docker `/.env` file you can configure:

* `APP_ID` - can be used inside `docker-compose` files to customize some volumes mapping to project-based specific folders.
* `HOST_WEB_PORT` - configuration of external mapped ports to the nginx server
* `HOST_API_PORT` - configuration of external mapped ports for the WebApp API Server

##### SSH RSA

Furthermore, to successfuly download private core repository you will need Private RSA key, which is configured 
inside your BitBucket account (you must configure [SSH connect](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) to your BitBucket Account).

**Important**: Your RSA key should NOT have a passphrase!

Once you configured your SSH key and checked that it's working (clone this repository using SSH option), you need to
copy your private RSA key to `runtime` folder and reset it's permissions to 644. 

If you used the standard name `id_rsa`, then you can just call a make command:  
_* You have to run this command in your original OS, and NOT inside Vagrant VM if you have Mac OS._

`make init-rsa`

If you have a custom name, then use this commands:

`cp -f /PATH/TO/PRIVATE/KEY ./runtime/id_rsa && chmod 644 ./runtime/id_rsa`

**Docker compose**

There are 2 docker-compose main files:

* `docker-compose.example` which is copied as `/docker-compose.yml`
* `build/docker-compose.*dev.yml` which is copied as `/docker-compose.override.yml` and merged automatically by docker compose utility. 
This file contains special configuration for development environment (like watch poll mode, extra volumes, etc.)

After you init your environment variables and configure your required ports (inside `.env`, `docker-compose.yml` and `configs/nginx-server.conf`)
you can do a test-run to check that your docker-compose configuration is correct:

#### 2. Docker config test

* First you need to do a docker login to https://hub.jcdev.net with your username/password (ask Server Administrator for it).  
`docker login hub.jcdev.net:24000`.

* Then you can check that config is correct.  
`docker-compose config`

If you see the final configuration array without error - you can continue to the next step.

#### 3. Installing LenderKit

To install the project after `init` and config test just run:

```bash
make install
```

After installation it will launch nginx container automatically pointed to the production dist files.  
You site will be available at http://anything-pointing-to-localhost:8080 (for example: http://localhost:8080).

**If you run project with docker - this is configured already.**

#### 4. Site Domain and Access

##### WebApp

On localhost you need to point your project domain to a localhost inside `/etc/hosts` file. 
By default, domain is `lenderkit-webapp.test`, so you need to add this to your hosts file (for Mac users - run in your terminal, NOT Vagrant):

```bash
sudo bash -c 'echo "127.0.0.1 lenderkit-webapp.test" >> /etc/hosts'
``` 

_or just edit this file with editor: `sudo nano /etc/hosts`_

By default, site is accessible within such URLs:

* https://lenderkit-webapp.test:8080

##### WebApp API Server

The server is available on the same URL as a WebApp, but has different port (by default it is 8081),
so you can send requests to it.

**Important**: the request to the API should contain a token in the `X-Csrf-Token` header.
The token can be found the in `api/.env`.

## Start, stop, update

Run the following commands on the **root level**

To run the project:
```bash
make run
```

To update your code:

```bash
make update
```

To stop the project:
```bash
make stop
```

## Docker containers

Default installation will launch such containers:

* `webapp-nginx`  - the web-server to serve app static built dist files and processes server api requests and forwards them to api node.js server application
* `webapp-app`    - the application itself
* `webapp-api`    - the application API (uses Node.js)

## Docker Node.js container

All js-relative scripts (such as npm) should be run inside the `app` docker container.
To open docker container bash you can run (on **root level**)

`make nodejs-bash`

`/app` folder contains it's own Makefile (**application level**), which simplify some standard commands inside docker node.js container.
After you opened `nodejs-bash` you can run `make` to get information of available commands.

The WebApp API Server has its own container - `webapp-api`.

## Read more
If you plan to develop this project we suggest to read all materials available in our [Detailed Documentation](docs/README.md).
