#!/bin/bash

PAYMENT_PROVIDERS=(gcen goji lemonway mangopay offline)
APP_SOURCCE="app"
WEBAPP_PREFIX="portal"
MAIN_VUE_APP_API_BASE_URL=$(cat app/.env | grep -e ^VUE_APP_API_BASE_URL)
MAIN_NGINX_BSE_URL=$(cat app/.env | grep VUE_APP_BASE_URL | sed 's%.*=%%g;s%^.*://%\.%g;;s%\.%\\\\\.%g;')
for PAYMENT_PROVIDER in ${PAYMENT_PROVIDERS[*]}
do
    VUE_APP_API_BASE_URL=$(cat app/.env | grep -e ^VUE_APP_API_BASE_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    VUE_APP_BASE_URL=$(cat app/.env | grep -e ^VUE_APP_BASE_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    echo $VUE_APP_API_BASE_URL
    cp "$APP_SOURCCE/.env.example" "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    echo "VUE_APP_OUTPUT_DIR" >> "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_BASE_URL=.*%'$VUE_APP_BASE_URL'%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_OUTPUT_DIR.*%VUE_APP_OUTPUT_DIR=dist/'$PAYMENT_PROVIDER'/html%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_API_BASE_URL=.*%'$VUE_APP_API_BASE_URL'%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_PAYMENT_GATEWAY=.*%VUE_APP_PAYMENT_GATEWAY='$PAYMENT_PROVIDER'%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_KYC_GATEWAY.*%VUE_APP_KYC_GATEWAY='$PAYMENT_PROVIDER'%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    sed 's%VUE_APP_PAYMENT_GATEWAY_HAS_ISA=.*%VUE_APP_PAYMENT_GATEWAY_HAS_ISA=false%' -i "$APP_SOURCCE/.env.$PAYMENT_PROVIDER.local"
    docker-compose run -T -w /var/www/html webapp-app bash -c "NODE_ENV=production npx vue-cli-service build --mode=$PAYMENT_PROVIDER"
done
cp -f configs/nginx-server.example.conf  configs/nginx-server.conf
echo $MAIN_NGINX_BSE_URL
sed 's%.*server_name.*%server_name "~^(?<subdomain>.+)'$MAIN_NGINX_BSE_URL'$";%' -i configs/nginx-server.conf
sed 's%root.*%root /var/www/$subdomain/html;%' -i configs/nginx-server.conf
make stop ||true
make run
