#!/bin/bash

echo "echo -e Host bitbucket.org\n\tStrictHostKeyChecking no\n >> /root/.ssh/config"
echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

echo "echo -e Host github.com\n\tStrictHostKeyChecking no\n >> /root/.ssh/config"
echo -e "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

if [ ! -f '/root/id_rsa' ]; then
  echo 'Mount your id_rsa connected to BitBucket to be able to download webapp-core from private repository.'
else
  cp ~/id_rsa ~/.ssh/id_rsa
  chmod 0600 ~/.ssh/id_rsa

  #remove comment below if you don't want to mount known_hosts file
  #ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
fi
