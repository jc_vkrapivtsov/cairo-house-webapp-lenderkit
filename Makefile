.PHONY: info init run test-run stop install update nodejs-bash npm-build npm-serve

NODEJS_CONTAINER = webapp-app
NODEJS_SERVER_CONTAINER = webapp-api

info:
	@echo "LenderKit WebApp Configuration/Launcher"
	@echo " "
	@echo "Usage:"
	@echo "	make command"
	@echo " "
	@echo "Options:"
	@echo "	NONE_INTERACTIVE=1	Use it in CI to prevent errors with docker-compose exec\run"
	@echo " "
	@echo "Available commands:"
	@echo "	init-dev 		Init configurations for development"
	@echo "	init 			Init configurations for setup on staging/production"
	@echo "	test-run		Docker run in place"
	@echo "	run	 		Docker run as daemon"
	@echo "	stop			Docker stop"
	@echo "	install	 		Run install process"
	@echo "	update	 		Run update process"
	@echo "	test	 		Run all necessary project tests on running containers"
	@echo "	chown	 		Return back correct file owner for files created inside a container"
	@echo "	nodejs-bash		Open nodejs container bash"
	@echo "	api-bash	Open the server webapp nodejs container bash"
	@echo "	npm-build	 	Build npm for production"
	@echo "	npm-watch	 	Run build in watch mode"
	@echo "	npm-multisite-build MSID={multisite-id}	 Build npm for multisite"

CURDIR_BASENAME = $(notdir ${CURDIR})

DOTENV = $(shell cat .env )

INSTALLATION_MODE = project
DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.dev.yml

ifeq "$(CURDIR_BASENAME)" "boilerplate"
	# mount directory with core and modules exists
	INSTALLATION_MODE = dev
	DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.webappdev.yml
endif

# check if we running a vagrant
ifeq "$(USER)" "vagrant"
	V = 1
endif

VAGRANT =
MAYBE_SUDO =
ifneq "$(V)" ""
	VAGRANT = 1
	MAYBE_SUDO = sudo
endif

DOCKER_T_FLAG =
ifeq "$(NON_INTERACTIVE)" "1"
    DOCKER_T_FLAG = -T
endif

DOCKER_COMPOSE_EXEC = docker-compose exec ${DOCKER_T_FLAG} --privileged --index=1
DOCKER_COMPOSE_EXEC_WWW = ${DOCKER_COMPOSE_EXEC} -w /var/www/html
DOCKER_COMPOSE_RUN = docker-compose run ${DOCKER_T_FLAG} -w /var/www/html

############################################
# Make Targets
############################################

init:
	@if [ ! -f '.env' ]; then \
		echo 'Copying .env file...'; \
		${MAYBE_SUDO} cp .env.example .env; \
	fi;
	@if [ ! -f 'app/.env' ]; then \
		echo 'Copying app/.env file...'; \
		${MAYBE_SUDO} cp app/.env.example app/.env; \
	fi;
	@if [ ! -f 'api/.env' ]; then \
		echo 'Copying api/.env file...'; \
		${MAYBE_SUDO} cp api/.env.example api/.env; \
	fi;
	@if [ ! -f 'docker-compose.yml' ]; then \
		echo 'Copying docker-compose.yml file...'; \
		${MAYBE_SUDO} cp docker-compose.example.yml docker-compose.yml; \
	fi;
	@if [ ! -f 'configs/nginx-server.conf' ]; then \
		echo 'Copying nginx config file...'; \
		${MAYBE_SUDO} cp ./configs/nginx-server.example.conf ./configs/nginx-server.conf; \
	fi;
	${MAYBE_SUDO} chmod +x ./build/nodejs/docker-bootstrap.sh

	@echo ''
	@echo 'NOTE: Please check your configuration in ".env" before run.'
	@echo 'NOTE: Please check your configuration in "app/.env" before run.'
	@echo 'NOTE: Please check your configuration in "api/.env" before run.'
	@echo 'NOTE: Please check your configuration in "docker-compose.yml" before run.'
	@echo 'NOTE: You can update nginx server configuration in "configs/nginx-server.conf".'
	@echo ''

init-dev: init
	@if [ ! -f 'docker-compose.override.yml' ]; then \
		echo 'Copying "docker-compose.override.yml" with dev mode envs...'; \
		${MAYBE_SUDO} cp ${DOCKER_COMPOSE_OVERRIDE_SRC} docker-compose.override.yml; \
	fi;

init-rsa:
	cp -f ~/.ssh/id_rsa ./runtime
	chmod 0644 ./runtime/id_rsa

init-runtime:
	if [ ! -d 'runtime' ]; then ${MAYBE_SUDO} mkdir runtime; fi

install: init-runtime
	docker-compose stop
	${DOCKER_COMPOSE_RUN} ${NODEJS_CONTAINER} bash -c "make install SKIP_VENDORS=${SKIP_VENDORS}"
	${DOCKER_COMPOSE_RUN} ${NODEJS_SERVER_CONTAINER} bash -c "make install SKIP_VENDORS=${SKIP_VENDORS}"
	${MAYBE_SUDO} touch runtime/installed
	echo '--- INSTALL COMPLETE ---'
	$(MAKE) run V=${VAGRANT}

update: init-runtime
	docker-compose stop
	@echo '--- UPDATE APP SERVICE ---'
	${DOCKER_COMPOSE_RUN} ${NODEJS_CONTAINER} bash -c "make update SKIP_VENDORS=${SKIP_VENDORS}"
	@echo '--- UPDATE API SERVICE ---'
	${DOCKER_COMPOSE_RUN} ${NODEJS_SERVER_CONTAINER} bash -c "make update SKIP_VENDORS=${SKIP_VENDORS}"
	echo '--- UPDATE COMPLETE ---'
	$(MAKE) run V=${VAGRANT}

run:
	docker-compose up --force-recreate --scale ${NODEJS_CONTAINER}=0 -d

test-run:
	docker-compose up --force-recreate --scale  ${NODEJS_CONTAINER}=0

stop:
	docker-compose down

test:

#######################
# Helpers
#######################

chown:
	sudo chown -R $$(whoami) .env .env.* docker-compose.* api/ app/ configs/

NODEJS_BASH_CMD = ${DOCKER_COMPOSE_RUN} ${NODEJS_CONTAINER} bash
NODEJS_SERVER_BASH_CMD = ${DOCKER_COMPOSE_RUN} ${NODEJS_SERVER_CONTAINER} bash

nodejs-bash:
	$(NODEJS_BASH_CMD)

api-bash:
	$(NODEJS_SERVER_BASH_CMD)

npm-build:
	$(NODEJS_BASH_CMD) -c 'npm run build'

npm-multisite-build:
	$(NODEJS_BASH_CMD) -c 'NODE_ENV=production npm run build -- --mode=${MSID}'

npm-watch:
	$(NODEJS_BASH_CMD) -c 'npm run watch'
